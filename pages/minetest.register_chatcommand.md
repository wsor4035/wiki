---
layout: page
title: minetest.register_chatcommand
permalink: /api/minetest.register_chatcommand
---

# **Minetest.register_chatcommand**

# Syntax

```lua
minetest.register_chatcommand("command_name", {chatcommand definition})
```

# Description

registers a chat command that can be ussed by issueing /`command_name` in chat  

# Parameters 

1. Name of the command
2. list containing the following
   * *params:* - required - string - parameter information, typically each one is put in <>. Used for /help
   * *description:* - required - string - a description of what the command does
   * *privs:* - optional - list of booleans, privs that the user must have to use the command
   * *func:* - required - function - return true for success and false for failure(inculde a string to note why)  
        - function(name, param) - name is the player name, and param is input after the command
        - do note that the first param of func may not be a player on the server, a example of this is the irc mod

# Example(s)

```lua
minetest.register_chatcommand("say", {
	params = "<text>",
	description = "Send text to chat",
	privs = {talk = true},
	func = function( _ , text)
		minetest.chat_send_all(text)
		return true, "Text was sent successfully"
	end,
})
```

```lua
minetest.register_chatcommand("test1", {
	params = "",
	description = "Test 1: Modify player's inventory view",
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, "Player not found"
		end
		player:set_inventory_formspec(
				"size[13,7.5]"..
				"image[6,0.6;1,2;player.png]"..
				"list[current_player;main;5,3.5;8,4;]"..
				"list[current_player;craft;8,0;3,3;]"..
				"list[current_player;craftpreview;12,1;1,1;]"..
				"list[detached:test_inventory;main;0,0;4,6;0]"..
				"button[0.5,7;2,1;button1;Button 1]"..
				"button_exit[2.5,7;2,1;button2;Exit Button]"
		)
		return true, "Done."
	end,
})
```
